<?php

namespace Drupal\related_nodes_block;

use Drupal\devutil\DevUtil;

/**
 * Provides constants and short helper methods for the module.
 */
class Module {
  /**
   * Machine name of the module.
   *
   * @var string NAME
   */
  const NAME = 'related_nodes_block';

  /**
   * Human-readable label of the module.
   *
   * @var string LABEL
   */
  const LABEL = 'Related Nodes Block';

  /**
   * Module webpage.
   *
   * @var string MODULE_WEBPAGE
   */
  const MODULE_WEBPAGE = 'https://www.drupal.org/project/' . self::NAME;

  /**
   * Only when true, debug function writes messages.
   *
   * @var boolean DEBUG_MODE
   */
  const DEBUG_MODE = FALSE;

  /**
   * Writes variable values to a static debug file.
   *
   * Requires \Drupal\devutil\DevUtil to work, otherwise silently bypasses.
   * This helps in debugging, but even if debug statements accidentally
   * remain in the code, it won't be a problem.
   */
  public static function debug($var, ...$params) {
    if (class_exists('\Drupal\devutil\DevUtil')) {
      DevUtil::debug($var, $params);
    }
  }

}
